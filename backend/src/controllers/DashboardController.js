const Debt = require('../models/Debt');

module.exports = {
    async show(req, res) {
        const { user_id } = req.headers;

        const debts = await Debt.find({ user: user_id });

        return res.json( debts );
    }
}
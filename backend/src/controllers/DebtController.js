//index, show, store, update, destroy
const User = require('../models/User');
const Debt = require('../models/Debt');

module.exports = {
    async index(req, res) {
        const { person } = req.query;
        
        const debts = await Debt.find({ person: person });

        return res.json(debts);
    },

    async store(req, res) {
        const { title, person, price } = req.body;
        //body vazio
        const { user_id } = req.headers;

        const user = await User.findById(user_id);

        if(!user) {
            return res.status(400).json({ error: 'User does not exists' }); 
        }

        const debt = await Debt.create({
            user: user_id,
            title,
            person: person,
            price
        })

        return res.json(debt)
    }
};
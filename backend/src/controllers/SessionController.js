//index, show, store, update, destroy
const User = require('../models/User');

module.exports = {
    async store(req, res) {
        const { email } = req.body;
    
        let user = await User.findOne({ email }) // { email: email } como a nomeclatura é a mesma é possível abreviar

        if(!user) {
            const user = await User.create({ email });
        }

        return res.json(user);
    }
};
const mongoose = require('mongoose');

const DebtSchema = new mongoose.Schema({
    title: String,
    price: Number,
    person: String,
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'  //model referido
    }
}, {
    toJSON: {
        virtuals: true,
    }
});

module.exports = mongoose.model('Debt', DebtSchema);
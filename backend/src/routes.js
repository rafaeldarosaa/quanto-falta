const express = require('express');

const SessionController = require('./controllers/SessionController');
const DebtController = require('./controllers/DebtController');
const DashboardController = require('./controllers/DashboardController');

const routes = express.Router();

routes.post('/sessions', SessionController.store);

routes.get('/debts', DebtController.index);
routes.post('/debts', DebtController.store);

routes.get('/dashboard', DashboardController.show);

module.exports = routes;
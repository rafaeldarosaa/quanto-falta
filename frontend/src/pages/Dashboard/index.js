import React, { useEffect, useState, useMemo } from 'react';
import { Link } from 'react-router-dom'
import socketio from 'socket.io-client';
import api from '../../services/api';
import './styles.css';

//continuar

export default function Dashboard() {
    const [debts, setDebts] = useState([]);
    const [requests, setRequests] = useState([]);

    const user_id = localStorage.getItem('user');
    const socket = useMemo(() => socketio('http://localhost:3333', {
        query: { user_id },
    }), [user_id]); //somente refaz a conexão se o user_id mudar

    useEffect(() => {
        async function loadDebts() {
            const user_id = localStorage.getItem('user');
            const response = await api.get('/dashboard', {
                headers: { user_id }
            })

            setDebts(response.data);
        }

        loadDebts();
    }, []);

    return (
        <>
            <ul className="debt-list">
                {debts.map(debt => (
                    <li key={debt._id}>
                        <headers/>
                        <strong>{debt.title}</strong>
                        <span>{debt.price ? `R$${debt.price}/dia` : `GRATUITO`}</span>
                    </li>
                    ))
                }
            </ul>

            <Link to="/new">
                <button className="btn">Cadastrar novo</button>
            </Link>
        </>
    )
}
import React, { useState, useMemo } from 'react';
import api from '../../services/api';

import camera from '../../assets/camera.svg';

import './styles.css';

export default function New({ history }) {
    const [title, setTitle] = useState('');
    const [person, setPerson] = useState('');
    const [price, setPrice] = useState('');

    async function handleSubmit(event) {
        event.preventDefault();
        
        const user_id = localStorage.getItem('user');
        const data = new FormData();

        data.append('title', title);
        data.append('person', person);
        data.append('price', price); 

        await api.post('/debts', data, {
            headers: { user_id }
        })
        
        history.push('/dashboard');
    }

    return (
        <form onSubmit={handleSubmit}>

            <label htmlFor="title">O QUE? *</label>
            <input
                id="title"
                placeholder="Objeto da cobrança"
                value={title}
                onChange={event => setTitle(event.target.value)}
            />

            <label htmlFor="person">PARA QUEM * </label>
            <input
                id="person"
                placeholder="Para quem?"
                value={person}
                onChange={event => setPerson(event.target.value)}
            />

            <label htmlFor="price">VALOR A RECEBER *</label>
            <input
                id="price"
                placeholder="Valor cobrado"
                value={price}
                onChange={event => setPrice(event.target.value)}
            />

            <button type="submit" className="btn">Cadastrar</button>
        </form>
    )
}